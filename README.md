[![pipeline status](https://gitlab.com/Hafthorat/hbv204m/badges/master/pipeline.svg)](https://gitlab.com/Hafthorat/hbv204m/-/commits/master)
[![coverage report](https://gitlab.com/Hafthorat/hbv204m/badges/master/coverage.svg)](https://gitlab.com/Hafthorat/hbv204m/-/commits/master)

[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=Hafthorat_hbv204m&metric=alert_status)](https://sonarcloud.io/summary/new_code?id=Hafthorat_hbv204m)

# About this project

This is a project for a software quality management course in Háskóli Íslands, 2022.

It is made for learning purposes, where various aspects of software quality management will be explored.

See [wiki](https://gitlab.com/Hafthorat/hbv204m/-/wikis/home) for further details about the management and other details of the project.

## Automated building

[Maven 3.8.4](https://maven.apache.org/download.cgi) and [Java SE Development Kit 8u181](https://www.oracle.com/java/technologies/javase/javase8-archive-downloads.html) is used for automated building. Other versions are likely to work, but results may vary.

See [Wiki page](https://gitlab.com/Hafthorat/hbv204m/-/wikis/Software-Quality-Management-Plan) for further details.
