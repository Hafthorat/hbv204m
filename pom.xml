<?xml version="1.0" encoding="UTF-8"?>

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <groupId>is.hi.cs.hbv204</groupId>
  <artifactId>junit</artifactId>
  <version>1.0-SNAPSHOT</version>

  <name>HBVM204-HAT</name>
  <description>Project for software quality management course at the University of Iceland, spring 2022</description>
  <url>https://gitlab.com/Hafthorat/hbv204m</url>
  <inceptionYear>2022</inceptionYear>

  <organization>
    <name>Háskóli Íslands</name>
    <url>https://www.hi.is/</url>
  </organization>

	<developers>
		<developer>
      <id>HAT</id>
			<name>Hafþór Aron Tómasson</name>
			<url>https://gitlab.com/Hafthorat</url>
      <email>hat16@hi.is</email>
      <roles>
        <role>Student</role>
      </roles>
		</developer>
	</developers>

  <licenses>
    <license>
      <name>Common Public License - v 1.0</name>
      <url>https://opensource.org/licenses/cpl1.0.txt</url>
    </license>
  </licenses>

  <properties>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    <java.version>8</java.version>
    <maven.compiler.source>${java.version}</maven.compiler.source>
    <maven.compiler.target>${java.version}</maven.compiler.target>
    <!--<maven.compiler.release>${java.version}</maven.compiler.release>-->
    <sonar.organization>hafthorat</sonar.organization>
  </properties>

  <dependencyManagement>
		<dependencies>
			<!-- JUnit5 consists of packages junit-jupiter 5.x.y and junit-platform 
				1.X.Y and the x and X need to fit together. This is achieved by this BOM -->
			<dependency>
				<groupId>org.junit</groupId>
				<artifactId>junit-bom</artifactId>
				<version>5.8.2</version>
				<type>pom</type>
				<scope>import</scope>
			</dependency>
		</dependencies>
	</dependencyManagement>

	<dependencies>

		<!-- junit-jupiter includes junit-jupiter-api/-engine/-params Needed for 
			the actual annotations and asserts -->
		<dependency>
			<groupId>org.junit.jupiter</groupId>
			<artifactId>junit-jupiter</artifactId>
			<scope>test</scope>
		</dependency>

		<!-- junit-platform-suite includes junit-platform-suite-api/-engine Needed 
			for the suite annotations -->
		<dependency>
			<groupId>org.junit.platform</groupId>
			<artifactId>junit-platform-suite</artifactId>
			<scope>test</scope>
		</dependency>

		<!-- assertThat and hamcrest matchers -->
		<dependency>
			<groupId>org.hamcrest</groupId>
			<artifactId>hamcrest</artifactId>
			<version>2.2</version>
			<scope>test</scope>
		</dependency>

	</dependencies>

  <build>
    <plugins>

      <!-- clean lifecycle, see https://maven.apache.org/ref/current/maven-core/lifecycles.html#clean_Lifecycle -->
      <plugin>
        <artifactId>maven-clean-plugin</artifactId>
        <version>3.1.0</version>
      </plugin>

      <!-- default lifecycle, jar packaging: see https://maven.apache.org/ref/current/maven-core/default-bindings.html#Plugin_bindings_for_jar_packaging -->
      <plugin>
        <artifactId>maven-resources-plugin</artifactId>
        <version>3.0.2</version>
      </plugin>

      <plugin>
        <artifactId>maven-compiler-plugin</artifactId>
        <version>3.8.0</version>
      </plugin>

      <plugin>
        <!--To understand JUnit 5, surefire plugin ≥2.22.0 is needed-->
        <artifactId>maven-surefire-plugin</artifactId>
        <version>2.22.1</version>
        <configuration>
          <!-- Includes can be overriden on commandline using mvn test -Dtest=NameOfTestClass 
            - however -Dtest does not work if the class contains a JUnit5-style suite -->
          <includes>
            <!--Default config tests all classes with names starting or ending with Test, Tests or TestCase -->
            <!--include>AlltestsUsingSelectClasses</include-->
            <include>/junit/**</include>
          </includes>
        </configuration>
      </plugin>

      <!-- Code coverage -->
      <plugin>
        <groupId>org.jacoco</groupId>
        <artifactId>jacoco-maven-plugin</artifactId>
        <version>0.8.7</version>
        <executions>
          <!-- Prepare-agent called at initialize phase in order to instrument code -->
          <execution>
            <id>pre-unit-test</id>
            <goals>
              <goal>prepare-agent</goal>
            </goals>
          </execution>
          
          <!-- Make report generation (in target/site/jacoco/) bound to test phase (instead of verify phase) -->
          <execution>
            <id>post-unit-test</id>
            <phase>test</phase>
            <goals>
              <goal>report</goal>
            </goals>
          </execution>
          
        </executions>
      </plugin>

      <plugin>
        <artifactId>maven-jar-plugin</artifactId>
        <version>3.0.2</version>
      </plugin>

      <plugin>
        <artifactId>maven-install-plugin</artifactId>
        <version>2.5.2</version>
      </plugin>

      <plugin>
        <artifactId>maven-deploy-plugin</artifactId>
        <version>2.8.2</version>
      </plugin>

      <!-- site lifecycle, see https://maven.apache.org/ref/current/maven-core/lifecycles.html#site_Lifecycle -->
      <plugin>
        <artifactId>maven-site-plugin</artifactId>
        <version>3.7.1</version>
      </plugin>

      <plugin>
        <artifactId>maven-project-info-reports-plugin</artifactId>
        <version>3.0.0</version>
      </plugin>

    </plugins>

  </build>

  <!-- Used by site and project info report plugin  -->
  <reporting>
    <plugins>
      <plugin>
        <groupId>org.jacoco</groupId>
        <artifactId>jacoco-maven-plugin</artifactId>
        <reportSets>
          <reportSet>
            <reports>
              <report>report</report>
            </reports>
          </reportSet>
        </reportSets>
      </plugin>
    </plugins>
  </reporting>
  
</project>
