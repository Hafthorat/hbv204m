package junit.framework;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

public class AssertTest {
    
    @Test
    public void failTest(){
        assertThrows(junit.framework.AssertionFailedError.class, () -> {
            junit.framework.Assert.fail();
        });
    }

    @Test
    public void failTestWithMessage(){
        String message = "Test message";
        assertThrows(junit.framework.AssertionFailedError.class, () -> {
            junit.framework.Assert.fail(message);
        });
    }

    @Test
    public void assertTrueTest(){
        junit.framework.Assert.assertTrue(true);
        assertThrows(junit.framework.AssertionFailedError.class, () -> {
            junit.framework.Assert.assertTrue(false);   
        });
    }

    @Test
    public void assertFalseTest(){
        assertThrows(junit.framework.AssertionFailedError.class, () -> {
            junit.framework.Assert.assertFalse(true);
        });
    }

    @Test
    public void assertEqualsTest() {
        junit.framework.Assert.assertEquals(1, 1);
        junit.framework.Assert.assertEquals(null, null);
        assertThrows(junit.framework.AssertionFailedError.class, () -> {
            junit.framework.Assert.assertEquals(1, 2);
        });
        assertThrows(junit.framework.AssertionFailedError.class, () -> {
            junit.framework.Assert.assertEquals(2, 1);
        });
    }

    @Test
    public void assertNotNullTest(){
        junit.framework.Assert.assertNotNull(1);
        assertThrows(junit.framework.AssertionFailedError.class, () -> {
            junit.framework.Assert.assertNotNull(null);
        });
    }

    @Test
    public void assertSameTest(){
        String testStringA = "string";
        String testStringB = "Another string";
        junit.framework.Assert.assertSame(testStringA, testStringA);
        assertThrows(junit.framework.AssertionFailedError.class, () -> {
            junit.framework.Assert.assertSame(testStringA, testStringB);
        });
    }

    @Test
    public void assertNotSameTest(){
        String testStringA = "string";
        String testStringB = "Another string";
        assertNotSame(testStringA, testStringB);
        assertThrows(junit.framework.AssertionFailedError.class, () -> {
            junit.framework.Assert.assertNotSame(testStringA, testStringA);
        });
    }
}
