package junit.extensions;

import junit.framework.T;
import junit.framework.TResult;
import junit.framework.TSuite;

/**
 * A TSuite for active Tests. It runs each
 * test in a separate thread and waits until all
 * threads have terminated.
 * -- Aarhus Radisson Scandinavian Center 11th floor
 */ 
public class ActiveTSuite extends TSuite {
	private volatile int fActiveTestDeathCount;

	public ActiveTSuite() {
	}
		
	public ActiveTSuite(Class theClass) {
		super(theClass);
	}
	
	public ActiveTSuite(String name) {
		super (name);
	}
	
	public ActiveTSuite(Class theClass, String name) {
		super(theClass, name);
	}
	
	public void run(TResult result) {
		fActiveTestDeathCount= 0;
		super.run(result);
		waitUntilFinished();
	}
	
	public void runTest(final T test, final TResult result) {
		Thread t= new Thread() {
			public void run() {
				try {
					// inlined due to limitation in VA/Java 
					//ActiveTestSuite.super.runTest(test, result);
					test.run(result);
				} finally {
					ActiveTSuite.this.runFinished();
				}
			}
		};
		t.start();
	}

	synchronized void waitUntilFinished() {
		while (fActiveTestDeathCount < testCount()) {
			try {
				wait();
			} catch (InterruptedException e) {
				return; // ignore
			}
		}
	}
	
	synchronized public void runFinished() {
		fActiveTestDeathCount++;
		notifyAll();
	}
}