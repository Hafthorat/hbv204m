package junit.extensions;

import junit.framework.Assert;
import junit.framework.T;
import junit.framework.TResult;

/**
 * A Decorator for Tests. Use TestDecorator as the base class
 * for defining new test decorators. Test decorator subclasses
 * can be introduced to add behaviour before or after a test
 * is run.
 *
 */
public class TDecorator extends Assert implements T {
	protected T fTest;

	public TDecorator(T test) {
		fTest= test;
	}
	/**
	 * The basic run behaviour.
	 */
	public void basicRun(TResult result) {
		fTest.run(result);
	}
	public int countTestCases() {
		return fTest.countTestCases();
	}
	public void run(TResult result) {
		basicRun(result);
	}

	public String toString() {
		return fTest.toString();
	}

	public T getTest() {
		return fTest;
	}
}