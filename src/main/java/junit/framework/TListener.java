package junit.framework;

/**
 * A Listener for test progress
 */
public interface TListener {
	/**
 	 * An error occurred.
 	 */
	public void addError(T test, Throwable t);
	/**
 	 * A failure occurred.
 	 */
 	public void addFailure(T test, AssertionFailedError t);  
	/**
	 * A test ended.
	 */
 	public void endTest(T test); 
	/**
	 * A test started.
	 */
	public void startTest(T test);
}