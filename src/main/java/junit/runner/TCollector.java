package junit.runner;

import java.util.Enumeration;
import junit.swingui.TSelector;


/**
 * Collects Test class names to be presented
 * by the TestSelector. 
 * @see TSelector
 */
public interface TCollector {
	/**
	 * Returns an enumeration of Strings with qualified class names
	 */
	public Enumeration collectTests();
}
