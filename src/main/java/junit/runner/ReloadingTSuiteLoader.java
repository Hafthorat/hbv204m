package junit.runner;

/**
 * A TestSuite loader that can reload classes.
 */
public class ReloadingTSuiteLoader implements TSuiteLoader {
	
	public Class load(String suiteClassName) throws ClassNotFoundException {
		return createLoader().loadClass(suiteClassName, true);
	}
	
	public Class reload(Class aClass) throws ClassNotFoundException {
		return createLoader().loadClass(aClass.getName(), true);
	}
	
	protected TCaseClassLoader createLoader() {
		return new TCaseClassLoader();
	}
}