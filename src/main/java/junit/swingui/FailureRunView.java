package junit.swingui;

import java.awt.Component;
import java.awt.Font;

import javax.swing.DefaultListCellRenderer;
import javax.swing.Icon;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import junit.framework.T;
import junit.framework.TFailure;
import junit.framework.TResult;
import junit.runner.BaseTRunner;


/**
 * A view presenting the test failures as a list.
 */
public class FailureRunView implements TRunView {
	JList fFailureList;
	TRunContext fRunContext;
	
	/**
	 * Renders TestFailures in a JList
	 */
	static class FailureListCellRenderer extends DefaultListCellRenderer {
		private Icon fFailureIcon;
		private Icon fErrorIcon;
		
		FailureListCellRenderer() {
	    		super();
	    		loadIcons();
		}
	
		void loadIcons() {
			fFailureIcon= TRunner.getIconResource(getClass(), "icons/failure.gif");
			fErrorIcon= TRunner.getIconResource(getClass(), "icons/error.gif");		
		}
						
		public Component getListCellRendererComponent(
			JList list, Object value, int modelIndex, 
			boolean isSelected, boolean cellHasFocus) {
	
		    Component c= super.getListCellRendererComponent(list, value, modelIndex, isSelected, cellHasFocus);
			TFailure failure= (TFailure)value;
			String text= failure.failedTest().toString();
			String msg= failure.exceptionMessage();
			if (msg != null) 
				text+= ":" + BaseTRunner.truncate(msg); 
	 
			if (failure.isFailure()) { 
				if (fFailureIcon != null)
		    		setIcon(fFailureIcon);
			} else {
		    	if (fErrorIcon != null)
		    		setIcon(fErrorIcon);
		    }
			setText(text);
			setToolTipText(text);
			return c;
		}
	}
	
	public FailureRunView(TRunContext context) {
		fRunContext= context;
		fFailureList= new JList(fRunContext.getFailures());
		fFailureList.setFont(new Font("Dialog", Font.PLAIN, 12));
 
		fFailureList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		fFailureList.setCellRenderer(new FailureListCellRenderer());
		fFailureList.setVisibleRowCount(5);

		fFailureList.addListSelectionListener(
			new ListSelectionListener() {
				public void valueChanged(ListSelectionEvent e) {
					testSelected();
				}
			}
		);
	}
	
	public T getSelectedTest() {
		int index= fFailureList.getSelectedIndex();
		if (index == -1)
			return null;
			
		ListModel model= fFailureList.getModel();
		TFailure failure= (TFailure)model.getElementAt(index);
		return failure.failedTest();
	}
	
	public void activate() {
		testSelected();
	}
	
	public void addTab(JTabbedPane pane) {
		JScrollPane scrollPane= new JScrollPane(fFailureList, ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		Icon errorIcon= TRunner.getIconResource(getClass(), "icons/error.gif");
		pane.addTab("Failures", errorIcon, scrollPane, "The list of failed tests");
	}
		
	public void revealFailure(T failure) {
		fFailureList.setSelectedIndex(0);
	}
	
	public void aboutToStart(T suite, TResult result) {
	}

	public void runFinished(T suite, TResult result) {
	}

	protected void testSelected() {
		fRunContext.handleTestSelected(getSelectedTest());
	}
}
