package junit.swingui;

import java.util.Vector;

import javax.swing.Icon;
import javax.swing.JTabbedPane;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.TreePath;

import junit.framework.T;
import junit.framework.TResult;

/**
 * A hierarchical view of a test run.
 * The contents of a test suite is shown
 * as a tree.
 */
public class THierarchyRunView implements TRunView {
	TSuitePanel fTreeBrowser;
	TRunContext fTestContext;
	
	public THierarchyRunView(TRunContext context) {
		fTestContext= context;
		fTreeBrowser= new TSuitePanel();
		fTreeBrowser.getTree().addTreeSelectionListener(
			new TreeSelectionListener() {
				public void valueChanged(TreeSelectionEvent e) {
					testSelected();
				}
			}
		);
	}
	
	public void addTab(JTabbedPane pane) { 
		Icon treeIcon= TRunner.getIconResource(getClass(), "icons/hierarchy.gif");
		pane.addTab("Test Hierarchy", treeIcon, fTreeBrowser, "The test hierarchy");
	}
	
	public T getSelectedTest() {
		return fTreeBrowser.getSelectedTest();
	}
	
	public void activate() {
		testSelected();
	}
	
	public void revealFailure(T failure) {
		JTree tree= fTreeBrowser.getTree();
		TTreeModel model= (TTreeModel)tree.getModel();
		Vector vpath= new Vector();
		int index= model.findTest(failure, (T)model.getRoot(), vpath);
		if (index >= 0) {
			Object[] path= new Object[vpath.size()+1];
			vpath.copyInto(path);
			Object last= path[vpath.size()-1];
			path[vpath.size()]= model.getChild(last, index);
			TreePath selectionPath= new TreePath(path);
			tree.setSelectionPath(selectionPath);
			tree.makeVisible(selectionPath);
		}
	}
	
	public void aboutToStart(T suite, TResult result) {
		fTreeBrowser.showTestTree(suite);
		result.addListener(fTreeBrowser);
	}

	public void runFinished(T suite, TResult result) {
		result.removeListener(fTreeBrowser);
	}

	protected void testSelected() {
		fTestContext.handleTestSelected(getSelectedTest());
	}
}
