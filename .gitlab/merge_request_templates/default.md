# Checklist for merge requests

## MR creator

- [] Compiles and runs locally.
- [] Contains Junit tests.
- [] Passes tests from testing tools (SonarCloud, Junit).

## MR reviewer

- [] Methods and variables have descriptive names.
- [] Methods are documented.
